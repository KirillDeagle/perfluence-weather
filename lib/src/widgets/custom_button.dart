import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key? key,
    required this.title,
    required this.onTap,
    this.disabled = false,
    this.icon,
  }) : super(key: key);

  final String title;
  final VoidCallback? onTap;
  final bool disabled;
  final IconData? icon;

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = ElevatedButton.styleFrom(
      minimumSize: const Size.fromHeight(50),
    );
    final onPressed = disabled ? null : onTap;
    final child = Text(
      title,
      style: const TextStyle(
        fontSize: 20.0,
      ),
    );

    if (icon != null) {
      return ElevatedButton.icon(
        onPressed: onPressed,
        style: style,
        icon: Icon(icon),
        label: child,
      );
    }

    return ElevatedButton(
      onPressed: onPressed,
      style: style,
      child: child,
    );
  }
}
