import 'package:flutter/material.dart';

class WidthLimiter extends StatelessWidget {
  const WidthLimiter({
    Key? key,
    required this.child,
    this.maxWidth = 500
  }) : super(key: key);

  final Widget child;
  final double maxWidth;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: maxWidth),
      child: child,
    );
  }
}
