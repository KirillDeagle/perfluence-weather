import 'package:flutter/material.dart';

/// !!! Copied from my other projects !!!
class ThemeStuff {
  const ThemeStuff({
    required this.mainColor,
  });

  final Color mainColor;

  ThemeData getThemeData(bool isDark) {
    final ColorScheme colorScheme = isDark ? ColorScheme.dark(primary: mainColor) : ColorScheme.light(primary: mainColor);

    ThemeData themeData = ThemeData.from(
      colorScheme: colorScheme,
      textTheme: textTheme(isDark),
      useMaterial3: true,
    );
    themeData = themeData.copyWith(
      elevatedButtonTheme: elevatedButtonTheme(isDark),
      floatingActionButtonTheme: floatingActionButtonTheme(isDark),
      inputDecorationTheme: inputDecorationTheme(isDark),
    );

    return themeData;
  }

  //

  TextTheme textTheme(bool isDark) => isDark ? Typography.whiteHelsinki : Typography.blackHelsinki;

  ElevatedButtonThemeData elevatedButtonTheme(bool isDark) => ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          primary: mainColor,
          onPrimary: (isDark && ThemeData.estimateBrightnessForColor(mainColor) == Brightness.dark) ? Colors.white : Colors.black,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
        ),
      );

  InputDecorationTheme inputDecorationTheme(bool isDark) => InputDecorationTheme(
        fillColor: isDark ? Colors.grey[900]! : Colors.grey[300]!,
        filled: false,
        labelStyle: TextStyle(
          color: isDark ? Colors.grey[300] : Colors.grey[900],
          fontSize: 18,
          fontWeight: FontWeight.w500,
        ),
        hintStyle: TextStyle(
          color: isDark ? Colors.grey[300] : Colors.grey[900],
          fontSize: 14,
          fontWeight: FontWeight.w400,
        ),
        alignLabelWithHint: false,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: mainColor, width: 1),
          borderRadius: BorderRadius.circular(8),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: mainColor, width: 2),
          borderRadius: BorderRadius.circular(8),
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(color: mainColor, width: 1),
          borderRadius: BorderRadius.circular(8),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.red, width: 1),
          borderRadius: BorderRadius.circular(8),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.red, width: 2),
          borderRadius: BorderRadius.circular(8),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey[300]!, width: 1),
          borderRadius: BorderRadius.circular(8),
        ),
      );

  FloatingActionButtonThemeData floatingActionButtonTheme(bool isDark) => FloatingActionButtonThemeData(
        backgroundColor: mainColor,
        foregroundColor: (isDark && ThemeData.estimateBrightnessForColor(mainColor) == Brightness.dark) ? Colors.white : Colors.black,
      );
}
