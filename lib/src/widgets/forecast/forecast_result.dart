import 'package:flutter/material.dart';

import 'package:perfluence_test/src/models/forecast_model.dart';

class ForecastResult extends StatelessWidget {
  const ForecastResult({
    Key? key,
    required this.forecast,
  }) : super(key: key);

  final Forecast forecast;

  String getTempStr(double temp) {
    return "${temp.round()}°C";
  }

  static const double hugeFontSize = 48;
  static const double bigFontSize = 36;
  static const double mediumFontSize = 30;
  static const double smallFontSize = 16;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              forecast.cityName,
              style: const TextStyle(
                fontSize: bigFontSize,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              getTempStr(forecast.currentTemp),
              style: const TextStyle(fontSize: hugeFontSize),
            ),
            const Text(
              'Температура',
              style: TextStyle(fontSize: smallFontSize),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  getTempStr(forecast.minTemp),
                  style: const TextStyle(fontSize: mediumFontSize),
                ),
                const Text(
                  'Мин',
                  style: TextStyle(fontSize: smallFontSize),
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  getTempStr(forecast.maxTemp),
                  style: const TextStyle(fontSize: mediumFontSize),
                ),
                const Text(
                  'Макс',
                  style: TextStyle(fontSize: smallFontSize),
                ),
              ],
            ),
          ],
        )
      ],
    );
  }
}
