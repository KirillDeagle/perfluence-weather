import 'package:flutter/material.dart';

import 'package:perfluence_test/src/widgets/custom_button.dart';

class ForecastErrorButtons extends StatelessWidget {
  const ForecastErrorButtons({
    Key? key,
    required this.errorMessage,
    required this.onReturn,
    required this.onRefresh,
  }) : super(key: key);

  final String errorMessage;
  final VoidCallback onReturn;
  final VoidCallback onRefresh;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          errorMessage,
          style: const TextStyle(
            color: Colors.red,
            fontSize: 20,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
          ),
        ),
        const SizedBox(height: 20, width: double.infinity),
        CustomButton(
          title: 'Попробовать снова',
          icon: Icons.refresh,
          onTap: onRefresh,
        ),
        const SizedBox(height: 20, width: double.infinity),
        CustomButton(
          title: 'Вернуться назад',
          icon: Icons.arrow_back,
          onTap: onReturn,
        ),
      ],
    );
  }
}
