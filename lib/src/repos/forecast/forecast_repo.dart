import 'dart:io';

import 'package:dio/dio.dart';
import 'package:riverpod/riverpod.dart';

import 'package:perfluence_test/src/constants.dart';
import 'package:perfluence_test/src/models/forecast_model.dart';
import 'package:perfluence_test/src/models/loading_error_model.dart';

final dioProvider = Provider.autoDispose<Dio>((ref) => Dio());

final forecastRepoProvider = Provider.autoDispose<ForecastRepo>((ref) => ForecastRepo(ref.read));

class ForecastRepo {
  final Reader _read;

  ForecastRepo(this._read);

  Future<Forecast> getForecast({
    String cityName = '',
  }) async {
    try {
      if (cityName.isEmpty) {
        throw const LoadingError(message: 'City name is required');
      }

      final response = await _read(dioProvider).get(
        'https://api.openweathermap.org/data/2.5/weather',
        queryParameters: {
          'apiKey': Constants.apiKey,
          'q': cityName,
          'lang': 'ru',
        },
      );

      if (response.statusCode == 200) {
        final result = Map<String, dynamic>.from(response.data ?? {});
        final apiStatusCode = result["cod"];
        if (apiStatusCode == 200) {
          return Forecast.fromMap(result);
        } else {
          final apiMessage = result["message"];
          throw LoadingError(message: apiMessage, statusCode: apiStatusCode);
        }
      } else {
        String? statusMessage = response.statusMessage;
        int? statusCode = response.statusCode;
        throw LoadingError(message: statusMessage ?? 'Unknown Error', statusCode: statusCode);
      }
    } on SocketException {
      throw const LoadingError(message: 'Check your connection');
    } on DioError catch (e) {
      String? statusMessage = e.response?.statusMessage;
      int? statusCode = e.response?.statusCode;
      throw LoadingError(message: statusMessage ?? 'Unknown Error', statusCode: statusCode);
    } catch(e) {
      throw const LoadingError(message: 'Unknown Error');
    }
  }
}
