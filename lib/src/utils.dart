
class Utils {
  // from kelvin to celsius
  static double fromKToC(double input) => input - kelvinZero;

  static const double kelvinZero = 273.15;
}