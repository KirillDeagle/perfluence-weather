import 'package:flutter/material.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:perfluence_test/src/pages/forecast_page.dart';
import 'package:perfluence_test/src/pages/welcome_page.dart';
import 'package:perfluence_test/src/widgets/theme_stuff.dart';

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const ThemeStuff themeStuff = ThemeStuff(mainColor: Colors.green);

    return ProviderScope(
      child: MaterialApp(
        theme: themeStuff.getThemeData(false),
        darkTheme: themeStuff.getThemeData(true),
        themeMode: ThemeMode.system,
        onGenerateRoute: (RouteSettings routeSettings) {
          return MaterialPageRoute<void>(
            settings: routeSettings,
            builder: (BuildContext context) {
              switch (routeSettings.name) {
                case WelcomePage.routeName:
                  return const WelcomePage();
                case ForecastPage.routeName:
                  return const ForecastPage(cityName: '');
                default:
                  return const WelcomePage();
              }
            },
          );
        },
      ),
    );
  }
}
