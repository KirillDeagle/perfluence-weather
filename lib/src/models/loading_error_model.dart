class LoadingError implements Exception {
  const LoadingError({
    required this.message,
    this.statusCode,
  });

  final String message;
  final int? statusCode;

  String parseToStr() {
    if (statusCode == null) {
      return message;
    } else {
      return '$statusCode - $message';
    }
  }
}
