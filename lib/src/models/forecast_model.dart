import 'package:perfluence_test/src/utils.dart';

class Forecast {
  const Forecast({
    required this.cityName,
    required this.currentTemp,
    required this.minTemp,
    required this.maxTemp,
  });

  final String cityName;
  final double currentTemp;
  final double minTemp;
  final double maxTemp;

  factory Forecast.fromMap(Map<String, dynamic> map) {
    return Forecast(
      cityName: map['name'] ?? 'Unknown City',
      currentTemp: Utils.fromKToC(map['main']?["temp"] ?? Utils.kelvinZero),
      minTemp: Utils.fromKToC(map['main']?["temp_min"] ?? Utils.kelvinZero),
      maxTemp: Utils.fromKToC(map['main']?["temp_max"] ?? Utils.kelvinZero),
    );
  }

  factory Forecast.placeholder() {
    return const Forecast(
      cityName: '',
      currentTemp: Utils.kelvinZero,
      minTemp: Utils.kelvinZero,
      maxTemp: Utils.kelvinZero,
    );
  }

  @override
  String toString() {
    return '$cityName ${currentTemp.round()} ${minTemp.round()} ${maxTemp.round()}';
  }
}
