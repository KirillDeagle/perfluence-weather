import 'package:flutter/material.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:perfluence_test/src/controllers/forecast/forecast_controller.dart';
import 'package:perfluence_test/src/pages/forecast_page.dart';
import 'package:perfluence_test/src/widgets/custom_button.dart';
import 'package:perfluence_test/src/widgets/dismiss_keyboard.dart';
import 'package:perfluence_test/src/widgets/width_limiter.dart';

class WelcomePage extends ConsumerStatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  static const routeName = '/welcome';

  @override
  ConsumerState<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends ConsumerState<WelcomePage> {
  final TextEditingController _nameController = TextEditingController();

  String get enteredCityName => _nameController.text;

  void openForecastPage() async {
    if (enteredCityName.isNotEmpty) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ref.read(forecastControllerProvider.notifier).fetchForecast(enteredCityName);
      });
      await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => ForecastPage(cityName: enteredCityName),
        ),
      );
      // drop input if user returned
      _nameController.text = '';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DismissKeyboard(
        child: Container(
          color: Colors.transparent,
          alignment: Alignment.center,
          padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
          child: SizedBox.expand(
            child: Center(
              child: SingleChildScrollView(
                controller: ScrollController(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(height: 20.0, width: double.infinity),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(Icons.wb_sunny, size: 80),
                        SizedBox(width: 20.0),
                        Icon(Icons.wb_cloudy_outlined, size: 80),
                      ],
                    ),
                    const SizedBox(height: 20.0, width: double.infinity),
                    WidthLimiter(
                      child: TextField(
                        controller: _nameController,
                        decoration: const InputDecoration(
                          labelText: 'Введите название города',
                          prefixIcon: Icon(Icons.search),
                        ),
                        onSubmitted: (value) {
                          openForecastPage();
                        },
                      ),
                    ),
                    const SizedBox(height: 20.0, width: double.infinity),
                    WidthLimiter(
                      child: CustomButton(
                        title: 'Поиск',
                        icon: Icons.search,
                        onTap: openForecastPage,
                      ),
                    ),
                    const SizedBox(height: 20.0, width: double.infinity),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
