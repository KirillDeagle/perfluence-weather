import 'dart:io';

import 'package:flutter/material.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:perfluence_test/src/controllers/forecast/forecast_controller.dart';
import 'package:perfluence_test/src/widgets/custom_button.dart';
import 'package:perfluence_test/src/widgets/forecast/forecast_error_buttons.dart';
import 'package:perfluence_test/src/widgets/forecast/forecast_result.dart';
import 'package:perfluence_test/src/widgets/width_limiter.dart';

class ForecastPage extends ConsumerWidget {
  const ForecastPage({
    Key? key,
    required this.cityName,
  }) : super(key: key);

  final String cityName;

  static const routeName = '/forecast';

  Future<void> onRefresh(WidgetRef ref) async {
    await ref.read(forecastControllerProvider.notifier).fetchForecast(cityName);
  }

  void onReturn(BuildContext context, WidgetRef ref) {
    ref.read(forecastControllerProvider.notifier).reset();
    Navigator.of(context).pop(true);
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final forecastState = ref.watch(forecastControllerProvider);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      extendBodyBehindAppBar: true,
      body: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.fromLTRB(30, 16, 30, 0),
        child: RefreshIndicator(
          onRefresh: () async => await onRefresh(ref),
          child: SizedBox.expand(
            child: Center(
              child: SingleChildScrollView(
                controller: ScrollController(),
                physics: const AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(height: 20.0, width: double.infinity),
                    if (forecastState.status.isLoading)
                      const Center(
                        child: CircularProgressIndicator(),
                      ),
                    if (forecastState.status.isFailed)
                      Center(
                        child: ForecastErrorButtons(
                          errorMessage: forecastState.error?.parseToStr() ?? 'Error',
                          onRefresh: () => onRefresh(ref),
                          onReturn: () => onReturn(context, ref),
                        ),
                      ),
                    if (forecastState.status.isLoaded)
                      Center(
                        child: WidthLimiter(
                          maxWidth: 400,
                          child: Column(
                            children: [
                              ForecastResult(forecast: forecastState.forecast),
                              const SizedBox(height: 20.0, width: double.infinity),
                              CustomButton(
                                title: 'Поиск',
                                icon: Icons.search,
                                onTap: () => onReturn(context, ref),
                              ),
                            ],
                          ),
                        ),
                      ),
                    const SizedBox(height: 20.0, width: double.infinity),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: (Platform.isWindows && (forecastState.status.isLoaded || forecastState.status.isFailed))
          ? FloatingActionButton(
              onPressed: () => onRefresh(ref),
              child: const Icon(Icons.refresh),
            )
          : null,
    );
  }
}
