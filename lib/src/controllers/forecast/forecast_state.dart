import 'package:perfluence_test/src/models/forecast_model.dart';
import 'package:perfluence_test/src/models/loading_error_model.dart';

enum ForecastStatus {
  initial,
  loading,
  loaded,
  failed;

  bool get isInitial => this == ForecastStatus.initial;
  bool get isLoading => this == ForecastStatus.initial || this == ForecastStatus.loading;
  bool get isLoaded => this == ForecastStatus.loaded;
  bool get isFailed => this == ForecastStatus.failed;
}

class ForecastState {
  const ForecastState({
    this.status = ForecastStatus.initial,
    required this.forecast,
    this.error,
  });

  final ForecastStatus status;
  final Forecast forecast;
  final LoadingError? error;

  factory ForecastState.initial() {
    return ForecastState(
      forecast: Forecast.placeholder(),
    );
  }

  ForecastState copyWith({
    ForecastStatus? status,
    Forecast? forecast,
    LoadingError? error,
  }) {
    return ForecastState(
      status: status ?? this.status,
      forecast: forecast ?? this.forecast,
      error: error ?? this.error,
    );
  }
}
