import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:perfluence_test/src/controllers/forecast/forecast_state.dart';
import 'package:perfluence_test/src/models/forecast_model.dart';
import 'package:perfluence_test/src/models/loading_error_model.dart';
import 'package:perfluence_test/src/repos/forecast/forecast_repo.dart';

final forecastControllerProvider = StateNotifierProvider.autoDispose<ForecastController, ForecastState>(
  (ref) => ForecastController(ref.watch(forecastRepoProvider)),
);

class ForecastController extends StateNotifier<ForecastState> {
  ForecastController(this._forecastRepo) : super(ForecastState.initial());

  final ForecastRepo _forecastRepo;

  Future<void> fetchForecast(String cityName) async {
    loading();

    try {
      final forecast = await _forecastRepo.getForecast(cityName: cityName);
      loaded(forecast);
    } on LoadingError catch (e) {
      failed(e);
    }
  }

  void loading() {
    state = state.copyWith(
      status: ForecastStatus.loading,
      forecast: Forecast.placeholder(),
      error: null,
    );
  }

  void loaded(Forecast forecast) {
    state = state.copyWith(
      status: ForecastStatus.loaded,
      forecast: forecast,
      error: null,
    );
  }

  void failed(LoadingError e) {
    state = state.copyWith(
      status: ForecastStatus.failed,
      forecast: Forecast.placeholder(),
      error: e,
    );
  }

  void reset() {
    state = ForecastState.initial();
  }
}
