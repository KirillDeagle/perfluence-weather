import 'package:flutter/material.dart';

import 'package:perfluence_test/src/app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const MyApp());
}
